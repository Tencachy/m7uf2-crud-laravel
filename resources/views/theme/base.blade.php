<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

  <title>Gatoteca - Javier Romero</title>
</head>

<body class="bg-cream">
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-ocre">
      <div class="container-fluid">
        <a class="navbar-brand logo" href="/"><img src="https://images.squarespace-cdn.com/content/v1/61583236df8ffd37e403bb6c/a33d27eb-c5eb-4d5b-8252-1b9d044ef380/logo+gatoteca_completo.png?format=1500w" alt="" style="height: 50px;"></a>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="/">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-btn" href="{{ route('gato.index')}}">Gatos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-btn" href="{{ route('libro.index')}}">Libros</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <section class="main">
  @yield('contingut')
  </section>

  @yield('footer')

</body>

</html>