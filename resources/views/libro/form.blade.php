@extends('../theme/base')

@section('contingut')
<div class="container py-5 text-center">
    @if(isset($libro))
    <h1>Editar libro</h1>
    @else
    <h1>Añadir libro</h1>
    @endif

    @if(isset($libro))
    <form action="{{route('libro.update', $libro)}}" method="post">
        @method('PUT')
    @else        
    <form action="{{ route('libro.store')}}" method="post">
    @endif

        @csrf
        <div class="my-3 text-start">
            <label for="title" class="form-label">Título</label>
            <input type="text" name="title" class="form-control" placeholder="Título del libro" value="{{old('title') ?? @$libro->title}}">
            @error('title')
                    <p class="form-text text-danger">{{$message}}</p>
            @enderror
        </div>
        <div class="my-3 text-start">
            <label for="author" class="form-label">Autor/autora</label>
            <input type="text" name="author" class="form-control" placeholder="Autor/autora del libro" value="{{old('author') ?? @$libro->author}}">
            @error('author')
                    <p class="form-text text-danger">{{$message}}</p>
            @enderror
        </div>
        <div class="row">
            <div class="my-3 col-12 col-md-6 text-start">
                <label for="genre" class="form-label">Género</label>
                <input type="text" name="genre" class="form-control" placeholder="Género del libro" value="{{old('genre') ?? @$libro->genre}}">
                @error('author')
                        <p class="form-text text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="my-3 col-12 col-md-6 text-start">
                <label for="isbn" class="form-label">ISBN</label>
                <input type="number" name="isbn" class="form-control" placeholder="ISBN" step="0,01" value="{{old('isbn') ?? @$libro->isbn}}">
                @error('isbn')
                        <p class="form-text text-danger">{{$message}}</p>
                @enderror
            </div>
        </div>
        <div class="my-3 text-start">
            <label for="synopsis" class="form-label">Sinopsis</label>
            <textarea name="synopsis" cols="30" rows="5" class="form-control" placeholder="Sinopsis del libro">{{old('synopsis') ?? @$libro->synopsis}}</textarea>
            @error('synopsis')
                <p class="form-text text-danger">{{$message}}</p>
            @enderror
        </div>

        @if(isset($libro))
        <button type="submit" class="btn btn-brown">Editar libro</button>
        @else
        <button type="submit" class="btn btn-brown">Añadir libro</button>
        @endif
        <a href="{{ route('libro.index')}}" class="btn btn-danger">Cancelar</a>
    </form>
</div>
@stop
