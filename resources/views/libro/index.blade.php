@extends('../theme/base')

@section('contingut')
    <div class="container text-center mt-5">
        <h1>Lista de libros disponibles</h1>
        <a href="{{ route('libro.create')}}" class="btn btn-brown">Añadir libro</a>

        <table class="table border-brown my-5">
            <thread>
                <th>Título</th>
                <th>Autor</th>
                <th>Género</th>
                <th>Acciones</th>
            </thread>
            <tbody>
                @forelse($libros as $item)
                <tr>
                    <td>{{$item->title}}</td>
                    <td>{{$item->author}}</td>
                    <td>{{$item->genre}}</td>
                    <td>
                        <a href="{{route('libro.edit', $item)}}" class="btn btn-warning">Editar <img src="{{ asset('/images/icons8-edit-50.png') }}" class="ps-1" style="height: 20px;"></a>
                        <form action="{{ route('libro.destroy', $item) }}" method="post" class="d-inline">
                            @method('DELETE')
                            @csrf
                            <button type="submit" onclick='return confirm("¿Seguro que quieres eliminar el libro?")' class="btn btn-danger">Eliminar <img src="{{ asset('/images/icons8-trash-50.png') }}" class="ps-1" style="height: 20px;"></button>                        
                        </form>
                    </td>
                </tr>
                @empty
                <td colspan="4">Actualmente no hay libros registrados</td>
                @endforelse
            </tbody>
        </table>
        {{$libros->links("pagination::bootstrap-4")}}
    </div>
    
@stop