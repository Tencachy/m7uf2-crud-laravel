@extends('theme/base')

<body>
    @section('contingut')
    <div class="container bg-cream">
        <div class="text-center mt-5 rounded-10 bg-light-br">
            <h1 class="pt-3">GATOTECA</h1>
            <p>¡Bienvenido/a a la Gatoteca! Un lugar donde podrás disfrutar de una lectura junto a unos felinos alucinantes.
                <br>Si te gusta algún libro siempre podrás comprarlo y, si te encariñas de algún gato, tendrás la oportunidad de adoptarlo.
            </p>
            <div class="row justify-content-center">
                <div class="card bg-ocre m-5" style="width: 18rem;">
                    <a href="{{ route('gato.index')}}"><img src="{{ asset('/images/cat.jpg') }}" class="card-img-top pt-3 rounded-10" alt=""></a>
                    <div class="card-body">
                        <h5 class="card-title">Gatos</h5>
                        <p class="card-text">Lista de gatos en la Gatoteca !Conócelos a todos!</p>
                        <a href="{{ route('gato.index')}}" class="btn btn-brown">Gatos</a>
                    </div>
                </div>
                <div class="card bg-ocre m-5" style="width: 18rem;">
                    <a href="{{ route('libro.index')}}"><img src="{{ asset('/images/library.jpg') }}" class="card-img-top pt-3 rounded-10" alt=""></a>
                    <div class="card-body">
                        <h5 class="card-title">Libros</h5>
                        <p class="card-text">Lista de libros disponibles !Descúbrelos todos!</p>
                        <a href="{{ route('libro.index')}}" class="btn btn-brown">Libros</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('footer')
    <footer class="bg-brown d-flex flex-wrap justify-content-between align-items-center py-3 mt-4 border-top">
        <div class="col-md-4 d-flex align-items-center justify-content-center">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link text-ocre" href="#"><img src="{{ asset('images/icons8-twitter-30.png')}}" alt=""></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-ocre" href="#"><img src="{{ asset('images/icons8-instagram-30.png')}}" alt=""></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-ocre" href="#"><img src="{{ asset('images/icons8-facebook-30.png')}}" alt=""></a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 d-flex align-items-center justify-content-center flex-column">
            <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
                <img src="https://images.squarespace-cdn.com/content/v1/61583236df8ffd37e403bb6c/a33d27eb-c5eb-4d5b-8252-1b9d044ef380/logo+gatoteca_completo.png?format=1500w" alt="" style="height: 50px;">
            </a>
            <p class="text-ocre pt-3">Ven y disfruta de una buena lectura con la mejor compañía</p>
        </div>
        <div class="col-md-4 d-flex align-items-center justify-content-center">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link text-ocre" href="{{ route('gato.index')}}">Gatos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-ocre" href="{{ route('libro.index')}}">Libros</a>
                </li>
            </ul>
        </div>
    </footer>
    @stop
</body>