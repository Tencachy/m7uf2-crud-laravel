@extends('../theme/base')

@section('contingut')
    <div class="container text-center mt-5">
        <h1>Lista de gatos</h1>
        <a href="{{ route('gato.create')}}" class="btn btn-brown">Añadir gato</a>

        <table class="table border-brown my-5">
            <thread>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Raza</th>
                <th>Acciones</th>
            </thread>
            <tbody>
                @forelse($gatos as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->age}}</td>
                    <td>{{$item->breed}}</td>
                    <td>
                        <a href="{{route('gato.edit', $item)}}" class="btn btn-warning">Editar <img src="{{ asset('/images/icons8-edit-50.png') }}" class="ps-1" style="height: 20px;"></a>
                        <form action="{{ route('gato.destroy', $item) }}" method="post" class="d-inline">
                            @method('DELETE')
                            @csrf
                            <button type="submit" onclick='return confirm("¿Seguro que quieres eliminar el gato?")' class="btn btn-danger">Eliminar <img src="{{ asset('/images/icons8-trash-50.png') }}" class="ps-1" style="height: 20px;"></button>
                        </form>
                    </td>
                </tr>
                
                @empty
                <td colspan="4">Actualmente no hay gatos registrados</td>
                @endforelse
            </tbody>
        </table>
        {{$gatos->links("pagination::bootstrap-4")}}
    </div>
    
@stop