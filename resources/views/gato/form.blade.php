@extends('../theme/base')

@section('contingut')
<div class="container py-5 text-center">
    @if(isset($gato))
    <h1>Editar datos del gato</h1>
    @else
    <h1>Añadir nuevo gato</h1>
    @endif

    @if(isset($gato))
    <form action="{{route('gato.update', $gato)}}" method="post">
        @method('PUT')
    @else        
    <form action="{{ route('gato.store')}}" method="post">
    @endif

        @csrf
        <div class="my-3 text-start">
            <label for="name" class="form-label">Nombre</label>
            <input type="text" name="name" class="form-control" placeholder="Nombre" value="{{old('name') ?? @$gato->name}}">
            @error('name')
                    <p class="form-text text-danger">{{$message}}</p>
            @enderror
        </div>
        <div class="row">
            <div class="my-3 text-start col-12 col-md-6">
                <label for="age" class="form-label">Edad</label>
                <input type="number" name="age" class="form-control" placeholder="Edad del gato" step="0,01" value="{{old('age') ?? @$gato->age}}">
                @error('age')
                        <p class="form-text text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="my-3 text-start col-12 col-md-6">
                <label for="breed" class="form-label">Raza</label>
                <input type="text" name="breed" class="form-control" placeholder="Raza del gato" value="{{old('breed') ?? @$gato->breed}}">
                @error('breed')
                        <p class="form-text text-danger">{{$message}}</p>
                @enderror
            </div>
        </div>
        <div class="my-3 text-start">
            <label for="comments" class="form-label">Comentarios</label>
            <textarea name="comments" cols="30" rows="5" class="form-control" placeholder="Comentarios del gato, su personalidad, características...">{{old('comments') ?? @$gato->comments}}</textarea>
            @error('comments')
                <p class="form-text text-danger">{{$message}}</p>
            @enderror
        </div>

        @if(isset($gato))
        <button type="submit" class="btn btn-brown">Editar gato</button>
        @else
        <button type="submit" class="btn btn-brown">Añadir gato</button>
        @endif
        <a href="{{ route('gato.index')}}" class="btn btn-danger">Cancelar</a>
    </form>
</div>
@stop
