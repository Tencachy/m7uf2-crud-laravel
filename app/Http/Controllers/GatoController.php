<?php

namespace App\Http\Controllers;

use App\Models\Gato;
use Illuminate\Http\Request;

class GatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gatos = Gato::Paginate(5);
        return view('gato.index')->with('gatos', $gatos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gato.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:20',
            'age' => 'required|lte:99',
            'breed' => 'required|max:20'
        ]);

        $gato = Gato::create($request->only('name', 'age', 'breed', 'comments'));
        return redirect()->route('gato.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gato  $gato
     * @return \Illuminate\Http\Response
     */
    public function show(Gato $gato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gato  $gato
     * @return \Illuminate\Http\Response
     */
    public function edit(Gato $gato)
    {
        return view('gato.form')->with('gato', $gato);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gato  $gato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gato $gato)
    {
        $request->validate([
            'name' => 'required|max:20',
            'age' => 'required|lte:99',
            'breed' => 'required|max:20'
        ]);

        $gato->name = $request['name'];
        $gato->age = $request['age'];
        $gato->breed = $request['breed'];
        $gato->comments = $request['comments'];
        $gato->save();

        return redirect()->route('gato.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gato  $gato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gato $gato)
    {
        $gato->delete();
        return redirect()->route('gato.index');
    }
}
